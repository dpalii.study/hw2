import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { HttpClientModule } from '@angular/common/http';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatDialogModule} from '@angular/material/dialog';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './pages/auth/auth.component';
import { AuthService } from './services/auth/auth.service';
import { NotesComponent } from './pages/notes/notes.component';
import { NotesService } from './services/notes/notes.service';
import { HttpInterceptorProviders } from './interceptors/interceptor-provider';
import { NoteEditorComponent } from './pages/note-editor/note-editor.component';
import { ContentComponent } from './pages/content/content.component';
import { NoteDetailsComponent } from './pages/note-details/note-details.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { ProfileService } from './services/profile/profile.service';
import { ConfirmDialogComponent } from './pages/confirm-dialog/confirm-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    NotesComponent,
    NoteEditorComponent,
    ContentComponent,
    NoteDetailsComponent,
    NotFoundComponent,
    ProfileComponent,
    ConfirmDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    HttpClientModule,
    MatListModule,
    MatDividerModule,
    MatSnackBarModule,
    MatDialogModule
  ],
  providers: [
    AuthService,
    NotesService,
    ProfileService,
    HttpInterceptorProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
