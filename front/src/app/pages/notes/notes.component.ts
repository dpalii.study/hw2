import { Component, OnInit } from '@angular/core';
import { NotesService } from 'src/app/services/notes/notes.service';


@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements OnInit {
  notes = [];
  error = '';

  constructor(private notesService: NotesService) { }

  ngOnInit(): void {
    this.notesService.getNotes().subscribe({
      next: data => {
        this.notes = data.notes;
        this.error = '';
      },
      error: err => {
        this.error = err;
      }
    });
  }

}
