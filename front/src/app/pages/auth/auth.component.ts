import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { repeatPasswordValidator } from 'src/app/validators/repeatPasswordValidator';


@Component({
  selector: 'app-login',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  path = '';
  action = '';
  isLoginPage = true;
  hide = true;
  hideRepeat = true;
  error = '';
  submitted = false;
  loading = false;
  onSubmit = null;
  loginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
    repeatPassword: ['', Validators.required]
  },
  {
    validators: repeatPasswordValidator
  });

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) {
    this.path = router.url;
    this.isLoginPage = this.path === '/login';
    if (this.isLoginPage) {
      this.onSubmit = this.onLogin;
      this.loginForm.controls.repeatPassword.disable();
      this.action = 'Sign in';
    }
    else {
      this.onSubmit = this.onRegister;
      this.loginForm.controls.repeatPassword.enable();
      this.action = 'Sign up';
    }
  }

  get loginFormControl(): { [key: string]: AbstractControl } {
    return this.loginForm.controls;
  }

  ngOnInit(): void {
  }

  onLogin(): void {
    this.submitted = true;
    this.loading = true;
    if (this.loginForm.valid) {
      const formData = this.loginForm.value;
      this.authService.login(formData.username, formData.password)
        .subscribe({
          next: data => {
            this.error = '';
            this.submitted = false;
            this.loading = false;

            const url = this.authService.redirectUrl || '/notes';
            this.authService.redirectUrl = null;
            this.router.navigateByUrl(url);
          },
          error: err => {
            this.error = err;
            this.loading = false;
          }
        });
    }
  }
  onRegister(): void {
    this.submitted = true;
    this.loading = true;
    if (this.loginForm.valid) {
      const formData = this.loginForm.value;
      this.authService.register(formData.username, formData.password)
        .subscribe({
          next: data => {
            this.error = '';
            this.submitted = false;
            this.loading = false;
            this.router.navigateByUrl('/login');
          },
          error: err => {
            this.error = err;
            this.loading = false;
          }
        });
    }
  }
}
