import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Note, NotesService } from 'src/app/services/notes/notes.service';

@Component({
  selector: 'app-note-editor',
  templateUrl: './note-editor.component.html',
  styleUrls: ['./note-editor.component.scss']
})
export class NoteEditorComponent implements OnInit {
  url = '';
  error = '';
  mode = '';
  title = '';
  id = '';
  submitted = false;
  note = null;
  textControl = new FormControl('', Validators.required);


  constructor(private router: Router, private route: ActivatedRoute, private notesService: NotesService) {
    this.url = router.url;

    if (this.url === '/notes/create') {
      this.title = 'New note';
      this.mode = 'create';
    }
    else {
      this.title = 'Edit note';
      this.mode = 'edit';
    }
  }

  ngOnInit(): void {
    if (this.mode === 'edit') {
      this.id = this.route.snapshot.paramMap.get('id');
      this.notesService.getNoteById(this.id).subscribe({
        next: data => {
          this.note = data;
          this.textControl.setValue(this.note.text);
        },
        error: err => {
          this.error = err;
        }
      });
    }
  }

  onSubmit(): void {
    if (this.mode === 'create') {
      this.notesService.addNote(this.textControl.value).subscribe({
        next: data => {
          this.router.navigateByUrl('/notes');
        },
        error: err => {
          this.error = err;
        }
      });
    }
    else {
      this.notesService.editNote(this.id, this.textControl.value).subscribe({
        next: data => {
          this.router.navigateByUrl(`/notes/${this.id}`);
        },
        error: err => {
          this.error = err;
        }
      });
    }
  }

}
