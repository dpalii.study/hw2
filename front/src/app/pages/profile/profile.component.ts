import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

import { ProfileService } from 'src/app/services/profile/profile.service';
import { repeatPasswordValidator } from 'src/app/validators/repeatPasswordValidator';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  loading = false;
  passwordLoading = false;
  profile = null;
  error = '';
  hideOld = true;
  hide = true;
  hideRepeat = true;
  changePasswordForm = this.fb.group({
    oldPassword: ['', Validators.required],
    password: ['', Validators.required],
    repeatPassword: ['', Validators.required]
  },
  {
    validators: repeatPasswordValidator
  });

  constructor(
    private profileService: ProfileService,
    private fb: FormBuilder,
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  get changePasswordControl(): {[key: string]: AbstractControl} {
    return this.changePasswordForm.controls;
  }

  ngOnInit(): void {
    this.profileService.getProfile().subscribe({
      next: data => {
        this.profile = data;
      },
      error: err => {
        this.error = err;
      }
    });
  }

  onDelete(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { data: 'Do you want to delete your profile?' });

    dialogRef.afterClosed().subscribe({
      next: answer => {
        if (answer) {
          this.loading = true;
          this.profileService.deleteProfile().subscribe({
            next: data => {
              this.router.navigateByUrl('/login');
              this.loading = false;
            },
            error: err => {
              this.error = err;
              this.loading = false;
            }
          });
        }
      }
    });
  }

  onLogout(): void {
    this.profileService.logout();
    this.router.navigateByUrl('/login');
  }

  onChangePassword(): void {
    if (this.changePasswordForm.valid) {
      this.passwordLoading = true;
      const oldPassword = this.changePasswordControl.oldPassword.value;
      const newPassword = this.changePasswordControl.password.value;
      this.profileService.changePassword(oldPassword, newPassword).subscribe({
        next: data => {
          this.snackBar.open('Password changed', 'Close', {duration: 5000});
          this.passwordLoading = false;
        },
        error: err => {
          this.snackBar.open(err, 'Close', {duration: 5000});
          this.passwordLoading = false;
        }
      });
    }
  }
}
