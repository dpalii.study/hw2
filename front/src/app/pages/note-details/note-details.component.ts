import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

import { Note, NotesService } from 'src/app/services/notes/notes.service';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-note-details',
  templateUrl: './note-details.component.html',
  styleUrls: ['./note-details.component.scss']
})
export class NoteDetailsComponent implements OnInit {
  error = '';
  id = '';
  note: Note;
  loading = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private notesService: NotesService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.updateNote();
  }

  onEdit(): void {
    this.router.navigateByUrl(this.router.url + '/edit');
  }

  toggleCompletion(): void {
    this.loading = true;
    this.notesService.toggleNoteCompletionById(this.id).subscribe({
      next: data => {
        this.updateNote();
        this.loading = false;
      },
      error: err => {
        this.error = err;
        this.loading = false;
      }
    });
  }

  onDelete(): void {

    const dialogRef = this.dialog.open(ConfirmDialogComponent, { data: 'Do you want to delete this note?' });

    dialogRef.afterClosed().subscribe({
      next: answer => {
        if (answer) {
          this.loading = true;
          this.notesService.deleteNoteById(this.id).subscribe({
            next: data => {
              this.router.navigateByUrl('/notes');
              this.loading = false;
            },
            error: err => {
              this.error = err;
              this.loading = false;
            }
          });
        }
      }
    });
  }

  updateNote(): void {
    this.loading = true;
    this.notesService.getNoteById(this.id).subscribe({
      next: data => {
        this.note = data;
        this.loading = false;
      },
      error: err => {
        this.error = err;
        this.loading = false;
      }
    });
  }


}
