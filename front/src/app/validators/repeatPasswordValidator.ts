import { FormGroup, ValidatorFn } from '@angular/forms';

export function repeatPasswordValidator(group: FormGroup): void {
    const pwControl = group.controls.password;
    const repeatPwControl = group.controls.repeatPassword;
    const error = (pwControl.value !== repeatPwControl.value) ? { passwordsDoNotMatch: true } : null;

    repeatPwControl.setErrors(error);

    return;
}
