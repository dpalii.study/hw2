import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent
} from '@angular/common/http';
import { AuthService } from '../services/auth/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private auth: AuthService) {}

    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        const jwt = this.auth.currentUserToken;

        const authRequest = req.clone({
            headers: req.headers.set('Authorization', 'Bearer ' + jwt)
        });
        return next.handle(authRequest);
    }
}
