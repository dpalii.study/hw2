import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';


const httpOptions = {
  headers: {
      'Content-Type': 'application/json'
  },
  reportProgress: true
};

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  private readonly url = 'http://localhost:8080/api/auth';
  public currentUserToken: string;
  public redirectUrl: string;

  constructor(private http: HttpClient) {
    this.currentUserToken = localStorage.getItem('jwt');
    this.redirectUrl = '/notes';
  }

  login(username: string, password: string): Observable<object> {
    const body = {
      username,
      password
    };

    return this.http.post<{jwt_token: string}>(this.url + '/login', body, httpOptions)
      .pipe(
        tap(data => {
          this.currentUserToken = data.jwt_token;
          localStorage.setItem('jwt', data.jwt_token);
          return data;
        }),
        catchError(this.handleError)
      );
  }

  register(username: string, password: string): Observable<object> {
    const body = {
      username,
      password
    };

    return this.http.post(this.url + '/register', body, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  handleError(error: HttpErrorResponse): Observable<never> {
    if (error.error instanceof ErrorEvent) {
      console.error('Client-side error: ', error.message);
      return throwError('Something went wrong; please try again later');
    }
    console.error(
      `Back-end error: ` +
      `status: ${error.status}, ` +
      `body: ${error.message}`
    );
    return throwError(error.error.message);
  }
}
