import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

const httpOptions = {
  headers: {
      'Content-Type': 'application/json'
  },
  reportProgress: true
};

@Injectable({
  providedIn: 'root'
})
export class NotesService {
  private readonly url = 'http://localhost:8080/api/notes';

  constructor(private http: HttpClient) { }

  getNotes(): Observable<{notes: Note[]}> {

    return this.http.get<{notes: Note[]}>(this.url, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  addNote(text: string): Observable<{message: string}> {
    const body = {
      text
    };
    return this.http.post<{message: string}>(this.url, body, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  editNote(id: string, text: string): Observable<{message: string}> {
    const body = {
      text
    };

    return this.http.put<{message: string}>(`${this.url}/${id}`, body, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  getNoteById(id: string): Observable<Note> {
    return this.http.get<{note: Note}>(`${this.url}/${id}`, httpOptions)
      .pipe(
        map(data => data.note),
        catchError(this.handleError)
      );
  }

  toggleNoteCompletionById(id: string): Observable<{message: string}> {
    return this.http.patch<{message: string}>(`${this.url}/${id}`, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  deleteNoteById(id: string): Observable<{message: string}> {
    return this.http.delete<{message: string}>(`${this.url}/${id}`, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  handleError(error: HttpErrorResponse): Observable<never> {
    if (error.error instanceof ErrorEvent) {
      console.error('Client-side error: ', error.message);
      return throwError('Something went wrong; please try again later');
    }
    console.error(
      `Back-end error: ` +
      `status: ${error.status}, ` +
      `body: ${error.message}`
    );
    return throwError(error.error.message);
  }
}

export interface Note {
  _id: string;
  text: string;
  createdDate: Date;
  completed: boolean;
}
