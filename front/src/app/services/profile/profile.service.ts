import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: {
      'Content-Type': 'application/json'
  },
  reportProgress: true
};

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  private readonly url = 'http://localhost:8080/api/users/me';
  constructor(private http: HttpClient) { }

  getProfile(): Observable<User> {
    return this.http.get<{user: User}>(this.url, httpOptions)
      .pipe(
        tap(data => {
          return data;
        }),
        map(data => data.user),
        catchError(this.handleError)
      );
  }

  deleteProfile(): Observable<{message: string}> {
    return this.http.delete<{message: string}>(this.url, httpOptions)
      .pipe(
        tap(data => {
          localStorage.removeItem('jwt');
          return data;
        }),
        catchError(this.handleError)
      );
  }

  changePassword(oldPassword: string, newPassword: string): Observable<{message: string}> {
    const body = {
      oldPassword,
      newPassword
    };

    return this.http.patch<{message: string}>(this.url, body, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  logout(): void {
    localStorage.removeItem('jwt');
  }

  handleError(error: HttpErrorResponse): Observable<never> {
    if (error.error instanceof ErrorEvent) {
      console.error('Client-side error: ', error.message);
      return throwError('Something went wrong; please try again later');
    }
    console.error(
      `Back-end error: ` +
      `status: ${error.status}, ` +
      `body: ${error.message}`
    );
    return throwError(error.error.message);
  }
}

export interface User {
  _id: string;
  username: string;
  createdDate: string;
}
