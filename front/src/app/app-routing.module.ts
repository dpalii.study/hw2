import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './guards/auth.guard';
import { AuthComponent } from './pages/auth/auth.component';
import { NotesComponent } from './pages/notes/notes.component';
import { NoteEditorComponent } from './pages/note-editor/note-editor.component';
import { ContentComponent } from './pages/content/content.component';
import { NoteDetailsComponent } from './pages/note-details/note-details.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { ProfileComponent } from './pages/profile/profile.component';

const routes: Routes = [
  {
    path: 'login',
    component: AuthComponent
  },
  {
    path: 'register',
    component: AuthComponent
  },
  {
    path: '',
    canActivate: [AuthGuard],
    component: ContentComponent,
    children: [
      {
        path: '',
        redirectTo: '/notes',
        pathMatch: 'full'
      },
      {
        path: 'notes',
        component: NotesComponent
      },
      {
        path: 'notes/create',
        component: NoteEditorComponent
      },
      {
        path: 'notes/:id',
        component: NoteDetailsComponent
      },
      {
        path: 'notes/:id/edit',
        component: NoteEditorComponent
      },
      {
        path: 'me',
        component: ProfileComponent
      },
      {
        path: '**',
        component: NotFoundComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
