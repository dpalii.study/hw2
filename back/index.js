const express = require('express');
const dotenv = require('dotenv');
const mongoose = require('mongoose');

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const noteRouter = require('./routers/noteRouter'); 

dotenv.config();
const app = express();
const port = process.env.PORT;
const mongoUrl = 'mongodb+srv://dpalii:qwer1qwer@moderndb.dhrds.mongodb.net/hw2?retryWrites=true&w=majority';

mongoose.connect(mongoUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
}, (err) => {
    if (err) 
        console.log(err);
});

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "GET, PATCH, PUT, POST, DELETE, OPTIONS");
    next(); 
});

app.use(express.json());

app.use('/api', authRouter);
app.use('/api', userRouter);
app.use('/api', noteRouter);

app.listen(port, () => {
    console.log(`Server is listening on ${port} port`);
});