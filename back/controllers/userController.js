const User = require('../models/user');
const Credentials = require('../models/credentials');
const getHash = require('../auxiliary/hasher');

class UserController {
    static async getMe(req, res) {
        res.status(200).json({user: req.user});
    }

    static async deleteMe(req, res) {
        const user = req.user;

        try {
            const [deletedUser, deletedCredentials] = await Promise.all([
                User.findByIdAndDelete(user._id).exec(),
                Credentials.findOneAndDelete({ username: user.username }).exec()
            ]);

            if (!deletedUser || !deletedCredentials) {
                res.status(400).json({ message: "User not found" });
            }
            else {
                res.status(200).json({ message: "Success" });
            }
        }
        catch(err) {
            console.log(err);
            res.status(500).json({ message: "Server error" });
        }
    }

    static async changePassword(req, res) {
        const user = req.user;
        const { oldPassword, newPassword } = req.body;

        if (!oldPassword) {
            res.status(400).json({ message: "Old password is missing" });
            return;
        }
        if (!newPassword) {
            res.status(400).json({ message: "New password is missing" });
            return;
        }

        try {
            const credentials = await Credentials.findOneAndUpdate({ 
                username: user.username, 
                password: getHash(oldPassword) 
            }, 
            { 
                password: getHash(newPassword) 
            }).exec();

            if (!credentials) {
                res.status(400).json({ message: "Old password is incorrect" });
                return;
            }
            res.status(200).json({ message: "Success" });
        }
        catch(err) {
            console.log(err);
            res.status(500).json({ message: "Server error" });
        }
    }
}

module.exports = UserController;