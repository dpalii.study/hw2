const Note = require('../models/note');

class NoteController {
    static async getNotes(req, res) {
        const userId = req.user._id;

        try {
            const notes = await Note.find({ userId: userId }).exec();
            res.status(200).json({ notes: notes });
        }
        catch(err) {
            console.log(err);
            res.status(500).json({ message: "Server error" });
        }
    } 

    static async postNote(req, res) {
        const userId = req.user._id;
        const { text } = req.body;

        if (!text) {
            res.status(400).json({ message: "Note has no text" });
            return;
        }

        const note = new Note({
            text: text,
            userId: userId,
            completed: false,
            createdDate: new Date()
        });

        try {
            await note.save();
            res.status(200).json({ message: "Success" });
        }
        catch(err) {
            console.log(err);
            res.status(500).json({ message: "Server error" });
        }
    }

    static async getNoteById(req, res) {
        const userId = req.user._id;
        const id = req.params.id;

        try {
            const note = await Note.findOne({
                _id: id,
                userId: userId
            }).exec();

            if (!note) {
                res.status(404).json({ 
                    message: `Note with ID ${id} not found or it doesn't belong to current user` 
                });
                return;
            }
            
            res.status(200).json({ note: note });
        }
        catch(err) {
            console.log(err);
            res.status(500).json({ message: "Server error" });
        }
    }

    static async updateNoteById(req, res) {
        const userId = req.user._id;
        const id = req.params.id;
        const { text } = req.body;

        try {
            const oldNote = await Note.findByIdAndUpdate({ 
                _id: id, 
                userId: userId 
            },
            {
                text: text
            }).exec();

            if (!oldNote) {
                res.status(404).json({ 
                    message: `Note with ID ${id} not found or it doesn't belong to current user` 
                });
                return;
            }

            res.status(200).json({ message: "Success" });
        }
        catch(err) {
            console.log(err);
            res.status(500).json({ message: "Server error" });
        }
    }

    static async changeNoteStatusById(req, res) {
        const userId = req.user._id;
        const id = req.params.id;

        try {
            const note = await Note.findOne({
                _id: id,
                userId: userId
            }).exec();

            if (!note) {
                res.status(404).json({
                    message: `Note with ID ${id} not found or it doesn't belong to current user` 
                });
                return;
            }

            await Note.findByIdAndUpdate(note._id, { 
                completed: !note.completed 
            }).exec();

            res.status(200).json({ message: "Success" });
        }
        catch (err) {
            console.log(err);
            res.status(500).json({ message: "Server error" });
        }
    }

    static async deleteNoteById(req, res) {
        const userId = req.user._id;
        const id = req.params.id;

        try {
            const note = await Note.findOneAndDelete({
                _id: id,
                userId: userId,
            }).exec();

            if (!note) {
                res.status(404).json({
                    message: `Note with ID ${id} not found or it doesn't belong to current user` 
                });
                return;
            }

            res.status(200).json({ message: "Success" });
        }
        catch (err) {
            console.log(err);
            res.status(500).json({ message: "Server error" });
        }
    }
}

module.exports = NoteController;