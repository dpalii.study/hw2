const Credentials = require('../models/credentials');
const User = require('../models/user');
const getHash = require('../auxiliary/hasher');

const jwt = require('jsonwebtoken');

class AuthController {
    static async register(req, res) {

        const { username, password } = req.body;
        
        if (!username) {
            res.status(400).json({ message: "Username not specified" });
            return;
        }

        if (await Credentials.findOne({ username: username }).exec()) {
            res.status(400).json({
                message: "Username is not unique"
            });
            return;
        }

        if (!password) {
            res.status(400).json({ message: "Password not specified" });
            return;
        }

        const passHash = getHash(password);

        const credentials = new Credentials({
            username: username,
            password: passHash
        });
        const user = new User({
            username: username,
            createdDate: new Date()
        });

        try {
            await Promise.all([
                await credentials.save(),
                await user.save()
            ]);
            res.status(200).json({
                message: 'Success'
            });
        }
        catch (err) {
            console.log(err);
            res.status(500).json({ message: "Server error" });
        }
    }

    static async login(req, res) {
        const { username, password } = req.body;

        if (!username) {
            res.status(400).json({ message: "Username not specified" });
            return;
        }

        if (!password) {
            res.status(400).json({ message: "Password not specified" });
            return;
        }

        const passHash = getHash(password);

        try {
            const data = await Credentials.findOne({
                username: username,
                password: passHash
            });
            if (data) {
                const secret = process.env.SECRET;
                const user = await User.findOne({ username: username }).exec();
                const token = jwt.sign(JSON.stringify(user), secret);

                res.status(200).json({ jwt_token: token });
            }
            else {
                res.status(400).json({ message: "Username or password is incorrect" });
            }
        }
        catch(err) {
            console.log(err);
            res.status(500).json({ message: "Server error" });
        }
    }
}

module.exports = AuthController;