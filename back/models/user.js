const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    createdDate: {
        type: Date,
        required: true,
        unique: false
    }
})

module.exports = mongoose.model('user', schema);