const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    userId: {
        type: String,
        required: true,
        unique: false
    },
    completed: {
        type: Boolean,
        required: true,
        unique: false
    },
    text: {
        type: String,
        required: true,
        unique: false
    },
    createdDate: {
        type: Date,
        required: true,
        unique: false
    }
});

module.exports = mongoose.model('note', schema);