const express = require('express');

const authController = require('../controllers/userController');
const authorize = require('../middlewares/authMiddleware');

const router = express.Router();

router.get('/users/me', authorize, authController.getMe);
router.delete('/users/me', authorize, authController.deleteMe);
router.patch('/users/me', authorize, authController.changePassword);

module.exports = router;