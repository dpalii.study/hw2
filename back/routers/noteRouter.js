const express = require('express');

const authorize = require('../middlewares/authMiddleware');
const validateId = require('../middlewares/idValidationMiddleware');
const noteController = require('../controllers/noteController');

const router = express.Router();

router.get('/notes', authorize, noteController.getNotes);
router.post('/notes', authorize, noteController.postNote);
router.get('/notes/:id', validateId, authorize, noteController.getNoteById);
router.put('/notes/:id', validateId, authorize, noteController.updateNoteById);
router.patch('/notes/:id', validateId, authorize, noteController.changeNoteStatusById);
router.delete('/notes/:id', validateId, authorize, noteController.deleteNoteById);

module.exports = router;