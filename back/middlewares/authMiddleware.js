const jwt = require('jsonwebtoken');

module.exports = function authorize(req, res, next) {
    const header = req.headers.authorization;

    if (!header) {
        res.status(401).json({ message: "No authorization header provided" });
        return;
    }

    const [, token] = header.split(' ');
    const secret = process.env.SECRET;

    try {
        req.user = jwt.verify(token, secret);
        next();
    }
    catch(err) {
        res.status(401).json({ message: "JWT is invalid" });
    }
}